import socket


class Master(object):
    ''' master '''

    def __init__(self, ip = None, port = None):
        print('+-----------------------------------------------------------+')
        print('|                        DISCLAIMER                         |')
        print('|ESTE PROGRAMA FUE REALIZADO CON PROPOSITOS ACADEMICOS      |\n'
              '|Y TRABAJA DE FORMA LOCAL CON EL UNICO OBJETIVO DE HACER    |\n'
              '|DEMOSTRACION DE COMO FUNCIONAN ESTE TIPO DE SISTEMAS.      |\n'
              '|NO NOS RESPONSABILIZAMOS DEL MAL USO QUE SE LE PUEDA DAR   |\n'
              '|A ESTE SOFTWARE BAJO NINGUNA CIRCUNSTANCIA, USALO BAJO     |\n'
              '|TU PROPIO RIESGO.                                          |')
        print('+-----------------------------------------------------------+')
        self.serverIp = ip if ip else '127.0.0.1'
        self.serverPort = port if port else 12345
        self.master = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            self.master.connect((self.serverIp, self.serverPort))
        except:
            exit('Couldnt connect to the server...')

    def exit(self):
        try:
            self.master.shutdown(socket.SHUT_RDWR)
            self.master.close()
        finally:
            exit()

    def cmd(self):
        while 1:
            try:
                self.master.sendall(' ')  # see if we have a connection
                cmd = raw_input('\troot[{}]: $ '.format(self.serverIp))
                if cmd:
                    self.master.sendall(cmd)
                    self.master.settimeout(0.1)
                    # wait for response from server
                    print self.master.recv(1024)
            except socket.timeout:
                pass
            except:
                self.exit()

    def login(self):
        try:
            self.master.sendall('master')
            print self.master.recv(1024)  # server is going to ask for password

            password = raw_input('\nPassword: ')
            self.master.sendall(password)
            response = self.master.recv(1024)

            if response == 'Access granted':
                self.cmd()
            else:
                print response
        except:
            self.exit()

    def run(self):
        self.login()


Master().run()
